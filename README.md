<div align=center>![Artemis Logo](/logo.png)</div>

# Build and Deployment
A Kotlin based tool for uploading artifacts to Nexus...

**Available features:**
 - Uploading generic artifacts with or without a version manifest
 
**Planned Features:**
 - Maven repo support
 - ??

**Usage:**
```
Usage: build-and-deployment [OPTIONS]

Options:
  -p, --project TEXT               Specifies a project to deploy
  -na, --nexus_address TEXT        Nexus URL
  -nu, --nexus_username TEXT       Nexus username with upload permissions
  -np, --nexus_password TEXT       Nexus password with upload permissions
  -nrn, --nexus_repo_name TEXT     Nexus repo name
  -nrt, --nexus_repo_type TEXT     Nexus repo type
  -a, --action TEXT                Action to be performed. Available actions:
                                   [uploadArtifact]
  -mf, --manifest                  With manifest?
  -af, --artifact TEXT             Artifact to deploy
  -vgs, --version_generation_strategy TEXT
                                   Version generation strategy. Available
                                   options: [date], [date_rc]
  -rcn, --rc_number INT            Release Candidate number for date_rc
                                   strategy
  -bnr, --no_banner                Disable the info banner
  -dr, --dry_run                   Print as much info as possible but don't
                                   actually do anything
  -wh, --with_hash                 Upload the artifact with an MD5 hash
  -h, --help                       Show this message and exit
```

**Example:**
```
java -jar bnd.jar \
--project someproject \
--nexus_address https://nexus.com \
--nexus_username admin \
--nexus_password pass \
--nexus_repo_name proj \
--nexus_repo_type RAW \
--action uploadArtifact \
--manifest \
--artifact proj.jar \
--version_generation_strategy date_rc \
--rc_number 69
--with_hash
```

A sample Nexus repo managed by this app can be found [here](https://nexus.districraft.org/#browse/browse:jar).
The [Artemis](https://gitlab.com/districraft/artemis) project is built on the [CI server](https://ci.districraft.org), uploaded to Nexus and then the manifest is 
used to show download links on the project's [Gatsby](https://gatsbyjs.org)-based website (website coming soon).

