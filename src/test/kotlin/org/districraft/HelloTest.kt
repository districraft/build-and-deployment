package org.districraft

import org.districraft.bnd.data.VersionManifest
import org.districraft.bnd.nexus.Nexus
import org.districraft.bnd.nexus.NexusCredentials
import org.districraft.bnd.nexus.NexusRepositoryType
import org.districraft.bnd.nexus.Project
import org.districraft.bnd.nexus.util.NexusNetworkException
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class HelloTest() {

    private val creds: NexusCredentials
    private val nexus: Nexus
    private val artemisProject: Project

    init {
        creds = NexusCredentials("admin", "9904041811Ro")
        nexus = Nexus("dcx-nexus", "https://nexus.districraft.org", creds)
        nexus.registerRepository("jar", NexusRepositoryType.RAW)

        artemisProject = Project("artemis")
    }

    @Test
    fun testFetchingManifests() {
        try {
            val artemisManifest: VersionManifest? = nexus.getFirstRepoByName("jar")?.getVersionManifestForProject(artemisProject.projectName)
            println(artemisManifest)

            assertNotNull(artemisManifest, "Manifest is null")

        } catch (ex: Exception) {
            println(ex.message)
            println("======================")
            ex.printStackTrace()
        }
    }
}
