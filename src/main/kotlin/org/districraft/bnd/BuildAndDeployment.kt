package org.districraft.bnd

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.types.int
import org.districraft.bnd.deployers.DeployRawArtifact
import org.districraft.bnd.nexus.Nexus
import org.districraft.bnd.nexus.NexusCredentials
import org.districraft.bnd.nexus.NexusRepositoryType
import org.districraft.bnd.util.BannerAndInfo
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.system.exitProcess

open class BuildAndDeployment: CliktCommand() {

    companion object {
        val appVersion: String = "0.0.1-RC1"
    }

    private val log: Logger = LoggerFactory.getLogger(BuildAndDeployment::class.java)

    val project: String? by option("-p", "--project", help="Specifies a project to deploy")

    val nexusAddress: String? by option("-na", "--nexus_address", help="Nexus URL")
    val nexusUsername: String? by option("-nu", "--nexus_username", help="Nexus username with upload permissions")
    val nexusPassword: String? by option("-np", "--nexus_password", help="Nexus password with upload permissions")
    val nexusRepoName: String? by option("-nrn", "--nexus_repo_name", help="Nexus repo name")
    val nexusRepoType: String? by option("-nrt", "--nexus_repo_type", help="Nexus repo type")

    val action: String? by option("-a", "--action", help="Action to be performed. Available actions: [uploadArtifact]")

    val withManifest: Boolean by option("-mf", "--manifest", help="With manifest?").flag(default = false)
    val artifact: String? by option("-af", "--artifact", help="Artifact to deploy")

    val versionGenerationStrategy: String? by option("-vgs",
        "--version_generation_strategy",
        help="Version generation strategy. Available options: [date], [date_rc]")

    val rcNumber: Int? by option("-rcn", "--rc_number", help="Release Candidate number for date_rc strategy").int()

    val disableBanner: Boolean by option("-bnr", "--no_banner", help="Disable the info banner").flag(default = false)
    val dryRun: Boolean by option("-dr", "--dry_run", help="Print as much info as possible but don't actually do anything").flag(default = false)

    val withHash: Boolean by option("-wh", "--with_hash", help="Upload the artifact with an MD5 hash").flag(default = false)

    override fun run() {
        if(project == null) { log.error("Must specify project"); exitProcess(128) }
        if(nexusUsername == null) { log.error("Must specify nexus username"); exitProcess(128) }
        if(nexusPassword == null) { log.error("Must specify nexus password"); exitProcess(128) }
        if(nexusAddress == null) { log.error("Must specify nexus address"); exitProcess(128) }
        if(nexusRepoName == null) { log.error("Must specify nexus repo name"); exitProcess(128) }
        if(nexusRepoType == null) { log.error("Must specify nexus repo type"); exitProcess(128) }
        if(action == null) { log.error("Must specify action"); exitProcess(128) }

        val repoType: NexusRepositoryType = NexusRepositoryType.valueOf(nexusRepoType ?: "")

        val credentials: NexusCredentials = NexusCredentials(nexusUsername ?: "", nexusPassword ?: "")
        val nexus: Nexus = Nexus("dcx-nexus", nexusAddress ?: "", credentials)

        nexus.registerRepository(nexusRepoName ?: "", repoType)

        if(!disableBanner) BannerAndInfo(log)

        log.info("Initializing nexus integration... [OK]")

        when(action) {
            "uploadArtifact" -> {
                if(artifact == null) {
                    log.error("Must specify artifact. Exiting.")
                    exitProcess(128)
                }

                if(versionGenerationStrategy.equals("date_rc") && rcNumber == null) {
                    log.error("The date_rc strategy requires an RC number specified with -rcn. Exiting.")
                    exitProcess(128)
                }

                when(repoType) {
                    NexusRepositoryType.RAW ->
                        DeployRawArtifact(withManifest, artifact, nexus.getFirstRepoByName(nexusRepoName ?: ""), project, versionGenerationStrategy, rcNumber, nexus, dryRun, withHash).execute()
                    else -> {
                        log.error("Action ${action} not available for repo type ${repoType.name}. Exiting.")
                        exitProcess(128)
                    }
                }
            }
            else -> {
                log.error("Unknown action ${action}. Exiting.")
                exitProcess(128)
            }
        }

        log.info("All actions have been completed successfully. Thank you for using Districraft Buld'n'Deployment tool.")
    }
}

open class BNDApp {
    companion object {
        @JvmStatic fun main(args: Array<String>) {
            BuildAndDeployment().main(args)
        }
    }
}
