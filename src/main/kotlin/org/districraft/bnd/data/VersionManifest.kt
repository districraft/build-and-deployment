package org.districraft.bnd.data

data class VersionManifest(var id: String, var friendlyName: String, var latestVersion: String, var versions: MutableList<Version>)