package org.districraft.bnd.data

data class Commit(val commitMessage: String, val commitAuthor: String, val commitHash: String)