package org.districraft.bnd.data

data class Version(val versionId: String, val releaseFile: String, val tags: List<String>, val commits: List<Commit>)