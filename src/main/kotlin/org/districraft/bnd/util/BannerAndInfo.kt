package org.districraft.bnd.util

import org.districraft.bnd.BuildAndDeployment
import org.slf4j.Logger

class BannerAndInfo(log: Logger) {
    init {
        log.info("""            ____  _     _        _ ____        _ _     _ """)
        log.info("""           |  _ \(_)___| |_ _ __(_) __ ) _   _(_) | __| |""")
        log.info("""           | | | | / __| __| '__| |  _ \| | | | | |/ _` |""")
        log.info("""           | |_| | \__ \ |_| |  | | |_) | |_| | | | (_| |""")
        log.info("""           |____/|_|___/\__|_|  |_|____/ \__,_|_|_|\__,_|""")
        log.info("===========================================================================")
        log.info("          Welcome to Districraft Build and Deployment Tool")
        log.info("                         Version: ${BuildAndDeployment.appVersion}")
        log.info("              To learn more run with '--help' or visit")
        log.info("        https://gitlab.com/districraft/build-and-deployment")
        log.info("===========================================================================")
        log.info("")
    }
}