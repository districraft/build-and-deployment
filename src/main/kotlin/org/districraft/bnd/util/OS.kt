package org.districraft.bnd.util

enum class OS {
    WINDOWS,
    LINUX,
    MAC,
    OTHER;

    companion object {
        fun getCurrentOs(): OS {
            val os = System.getProperty("os.name").toLowerCase()

            return when {
                os.contains("win") -> {
                    OS.WINDOWS
                }
                os.contains("nix") || os.contains("nux") || os.contains("aix") -> {
                    OS.LINUX
                }
                os.contains("mac") -> {
                    OS.MAC
                }
                else -> OS.OTHER
            }
        }

        fun getOsTempDir(): String {
            val os = getCurrentOs()

            return when(os){
                WINDOWS -> "C:\\temp"
                LINUX, MAC -> "/tmp"
                else -> "./temp"
            }
        }
    }
}