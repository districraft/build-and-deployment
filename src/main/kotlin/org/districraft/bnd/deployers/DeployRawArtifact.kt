package org.districraft.bnd.deployers

import com.google.gson.Gson
import okhttp3.*
import org.apache.commons.io.FilenameUtils
import org.districraft.bnd.data.Commit
import org.districraft.bnd.data.Version
import org.districraft.bnd.data.VersionManifest
import org.districraft.bnd.deployers.util.DateRcVersionNumber
import org.districraft.bnd.deployers.util.DateVersionNumber
import org.districraft.bnd.nexus.Nexus
import org.districraft.bnd.nexus.NexusRepository
import org.districraft.bnd.nexus.util.MD5Hash
import org.districraft.bnd.util.OS
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.lang.Exception
import java.nio.charset.Charset
import java.nio.file.Paths
import kotlin.system.exitProcess

class DeployRawArtifact(
    private val withManifest: Boolean,
    private val artifact: String?,
    private val nexusRepo: NexusRepository?,
    private val project: String?,
    private val versionGenerationStrategy: String?,
    private val rcNumber: Int?,
    private val nexus: Nexus,
    private val dryRun: Boolean = false,
    private val withHash: Boolean = false
) {

    private val httpClient: OkHttpClient = OkHttpClient()
    private val log: Logger = LoggerFactory.getLogger(DeployRawArtifact::class.java)
    private val NEXUS_API_SEGMENT: String =
        "service/rest/v1/components?repository=${nexusRepo?.repoName?.toLowerCase()}"

    fun execute() {
        val baseVersionString: String = when (versionGenerationStrategy) {
            "date_rc" -> DateRcVersionNumber(rcNumber).get()
            "date" -> DateVersionNumber().get()
            else -> {
                log.error("Invalid version strategy.")
                exitProcess(128)
            }
        }

        val artifactExtension: String = FilenameUtils.getExtension(artifact)
        val artifactBaseName: String = FilenameUtils.getBaseName(artifact)
        val artifactFinalName: String = "$artifactBaseName-$baseVersionString.$artifactExtension"

        log.info("Preparing to upload '${artifactFinalName}'")

        val artifactFile: File = File(artifact ?: "")

        if (!artifactFile.exists()) {
            log.error("The specified artifact does not exist! Exiting.")
            exitProcess(128)
        }

        if (nexusRepo == null) {
            log.error("Invalid repository")
            exitProcess(128)
        }

        val requestBuilder: MultipartBody.Builder = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart(
                "raw.asset1",
                artifactFinalName,
                RequestBody.create(MediaType.parse("application/octet-stream"), artifactFile)
            )
            .addFormDataPart("raw.directory", project ?: "")
            .addFormDataPart("raw.asset1.filename", artifactFinalName)

        if(withHash) {
            val tempDir: File = Paths.get(OS.getOsTempDir(), project).toFile()

            if(tempDir.exists()) {
                tempDir.delete()
                tempDir.mkdir()
            } else {
                tempDir.mkdir()
            }

            val hashFile: File = Paths.get(OS.getOsTempDir(), project, "${artifactFinalName}.md5").toFile()
            log.debug("Manifest file is: ${hashFile.absolutePath}")
            hashFile.createNewFile()
            val afFile: File = File(artifact ?: "")

            val hash: String = MD5Hash(afFile.readBytes()).get()
            hashFile.writeText(hash, Charset.forName("utf-8"))

            requestBuilder.addFormDataPart(
                "raw.asset2",
                "${artifactFinalName}.md5",
                RequestBody.create(MediaType.parse("text/plain"), hashFile))
                .addFormDataPart("raw.asset2.filename", "${artifactFinalName}.md5")
        }

        val requestBody: RequestBody = requestBuilder.build()

        val finalUploadUrl: String = "${nexus.nexusAddress}/$NEXUS_API_SEGMENT"

        log.debug("Final upload URL: $finalUploadUrl")

        val request: Request = Request.Builder()
            .url(finalUploadUrl)
            .post(requestBody)
            .header("Authorization", Credentials.basic(nexus.credentials.username, nexus.credentials.password))
            .build()

        log.info("Uploading...")

        if (!dryRun) {

            val res: Response = httpClient.newCall(request).execute()

            when (res.code()) {
                403 -> {
                    log.error("Insufficient permission error when uploading artifact")
                    log.debug(res.body()?.string())
                    exitProcess(128)
                }
                422 -> {
                    log.error("Parameter 'repository' is required")
                    log.debug(res.body()?.string())
                    exitProcess(128)
                }
                200, 204 -> {
                    log.debug(res.body()?.string())
                    log.debug(res.code().toString())
                    log.info("Artifact uploaded successfully.")
                }
                else -> {
                    log.error("Unknown error when uploading artifact")
                    log.error(res.body()?.string())
                    log.error(res.code().toString())
                    exitProcess(128)
                }
            }
        } else {
            log.info("Dry run. Not actually uploading anything.")
        }

        if (withManifest) {
            log.info("Preparing to update manifest.")
            val originalManifest: VersionManifest = nexusRepo.getVersionManifestForProject(project)
            log.info("Found original manifest.")
            log.debug(originalManifest.toString())
            log.info("Updating manifest with following coordinates:")
            log.info("Latest Version: ${baseVersionString}")
            log.info("Next Version:")
            log.info("    Version ID: ${baseVersionString}")
            log.info("    Release File: ${artifactFinalName}")
            log.info("    Tags: [ EXPERIMENTAL ]")
            log.info("    Commits: N/A (not supported yet)")


            val fakeCommit: Commit = Commit("N/A", "N/A", "N/A")
            val version: Version =
                Version(baseVersionString, artifactFinalName, mutableListOf("EXPERIMENTAL"), mutableListOf(fakeCommit))
            originalManifest.id = baseVersionString
            originalManifest.friendlyName = baseVersionString
            originalManifest.latestVersion = baseVersionString
            originalManifest.versions.add(version)

            log.debug("New manifest:")
            log.debug(originalManifest.toString())

            log.info("Uploading manifest...")

            val JSON: MediaType? = MediaType.parse("application/json; charset=utf-8")
            val manifestSequestBody: RequestBody = RequestBody.create(JSON, Gson().toJson(originalManifest))

            val manifestUploadUrl: String =
                "${nexus.nexusAddress}/repository/${nexusRepo.repoName.toLowerCase()}/$project/versionManifest.json"

            log.debug("Uploading manifest to: ${manifestUploadUrl}")

            val manifestRequest: Request = Request.Builder()
                .header("Authorization", Credentials.basic(nexus.credentials.username, nexus.credentials.password))
                .put(manifestSequestBody)
                .url(manifestUploadUrl)
                .build()

            var resBody: String? = null

            if (!dryRun) {
                try {
                    val response: Response = httpClient.newCall(manifestRequest).execute()
                    if (response.body() == null) {
                        throw Exception()
                    }
                    if (!response.isSuccessful) {
                        log.error("Error while uploading manifest: ${response.code().toString()}")
                        exitProcess(128)
                    }

                    resBody = response.body()?.string()
                    log.debug(response.code().toString())
                } catch (e: Exception) {
                    log.error("Unknown error while uploading manifest.")
                    exitProcess(128)
                }

                log.debug(resBody)
            } else {
                log.info("Dry Run. Not uploading anything.")
            }
            log.info("Manifest has been successfully uploaded.")
        }
    }
}