package org.districraft.bnd.deployers.util

import java.text.SimpleDateFormat
import java.util.*

class DateRcVersionNumber(private val rcNumber: Int?) {
    fun get(): String {
        val datePortion: String = SimpleDateFormat("yyyy.MM.dd").format(Date())
        val rc: Int = this.rcNumber ?: 0
        return "$datePortion-RC$rcNumber"
    }
}
