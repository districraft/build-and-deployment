package org.districraft.bnd.deployers.util

import java.text.SimpleDateFormat
import java.util.*

class DateVersionNumber {
    fun get(): String {
        return SimpleDateFormat("yyyy.MM.dd").format(Date())
    }
}
