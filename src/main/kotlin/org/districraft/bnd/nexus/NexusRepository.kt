package org.districraft.bnd.nexus

import com.google.gson.Gson
import okhttp3.Credentials
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.districraft.bnd.data.VersionManifest
import org.districraft.bnd.nexus.util.NexusNetworkException

data class NexusRepository(val repoName: String, val repoType: NexusRepositoryType, val nexus: Nexus) {

    private val MANIFEST_NAME: String = "versionManifest.json"

    private val httpClient: OkHttpClient = OkHttpClient()

    fun getVersionManifestForProject(project: String?): VersionManifest {
        val finalAddress: String = "${nexus.nexusAddress}/repository/${repoName}/${project}/${MANIFEST_NAME}"

        val request: Request = Request.Builder()
            .url(finalAddress)
            .header("Authorization", Credentials.basic(nexus.credentials.username, nexus.credentials.password))
            .build()

        try {
            val response: Response = httpClient.newCall(request).execute()

            if(response.body() == null) throw NexusNetworkException("Could not fetch manifest - body is null")

            return Gson().fromJson(response.body()?.string(), VersionManifest::class.java)
        } catch (e: Exception) {
            throw NexusNetworkException("Could not fetch manifest - ${e.message}")
        }
    }
}