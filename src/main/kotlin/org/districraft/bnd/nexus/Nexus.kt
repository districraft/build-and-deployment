package org.districraft.bnd.nexus

data class Nexus (val nexusName: String, val nexusAddress: String, val credentials: NexusCredentials) {

    private val repos: ArrayList<NexusRepository> = ArrayList<NexusRepository>()

    fun registerRepository(repoName: String, repoType: NexusRepositoryType) {
        repos.add(NexusRepository(repoName, repoType, this))
    }

    fun getFirstRepoByType(type: NexusRepositoryType): NexusRepository? {
        return repos.find { it.repoType == type }
    }

    fun getFirstRepoByName(name: String): NexusRepository? {
        return repos.find { it.repoName == name }
    }
}