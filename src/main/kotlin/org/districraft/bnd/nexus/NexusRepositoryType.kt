package org.districraft.bnd.nexus

enum class NexusRepositoryType {
    RAW,
    MAVEN
}