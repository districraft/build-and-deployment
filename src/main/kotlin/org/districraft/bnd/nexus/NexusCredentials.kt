package org.districraft.bnd.nexus

data class NexusCredentials(val username: String, val password: String)