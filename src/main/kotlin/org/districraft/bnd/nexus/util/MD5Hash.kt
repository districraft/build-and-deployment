package org.districraft.bnd.nexus.util

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.security.MessageDigest
import javax.xml.bind.DatatypeConverter

class MD5Hash(private val file: ByteArray) {

    private val log: Logger = LoggerFactory.getLogger(MD5Hash::class.java)

    public fun get(): String {
        val hash: String = DatatypeConverter.printHexBinary(MessageDigest.getInstance("MD5").digest(file))
        log.debug("Hash is: ${hash}")
        return hash
    }
}